import { Component, OnInit } from '@angular/core';
import { Issue } from '../../models/issue';
import { IssuesService } from '../../services/issues.service';

@Component({
    selector: 'ap-issue-list',
    templateUrl: './issue-list.component.html',
    styleUrls: ['./issue-list.component.scss'
    ]
})
export class IssueListComponent implements OnInit {
    issues: Issue[] = [];

    selectedIssue: Issue | null = null;

    showDialog = false;

    showReportIssue = false;

    constructor(private issuesService: IssuesService) { }

    ngOnInit(): void {
        this.getIssues();
    }

    onCloseReport() {
        this.showReportIssue = false;
        this.getIssues();
        this.selectedIssue = null;
    }

    onConfirm(confirmed: boolean) {
        if (confirmed && this.selectedIssue) {
            this.issuesService.completeIssue(this.selectedIssue);
            this.getIssues();
        }
        this.selectedIssue = null;
        this.showDialog = false;
    }

    private getIssues() {
        this.issues = this.issuesService.getPendingIssues();
    }

}
