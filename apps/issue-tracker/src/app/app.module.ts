import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

const ROUTES: Routes = [{
    path: '',
    loadChildren: () => import('@ap/issues').then(m => m.IssuesModule),
}];

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, BrowserAnimationsModule, RouterModule.forRoot(ROUTES)],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {
}
