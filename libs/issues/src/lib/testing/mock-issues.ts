import { Issue } from '../models/issue';

export const issues: Issue[] = [{
    issueNo: '7fe81237-d1dd-4e04-b183-dcbd805ad787',
    title: 'Add email validation in registration form',
    description: 'Validate the email entered in the user registration form',
    priority: 'high',
    type: 'Feature'
}, {
    issueNo: 'cb0f8c2b-d867-49dd-9064-644a4ff21605',
    title: 'Display the adress details of a customer',
    description: 'Add a column to display the details of the customer address in the customer list',
    priority: 'low',
    type: 'Feature'
}, {
    issueNo: '23567089-5f37-49e7-af1d-28719cedfa5b',
    title: 'Export to CSV is not working',
    description: 'The export process of a report into CSV format throws an error',
    priority: 'high',
    type: 'Bug'
}, {
    issueNo: '80254e03-38da-4096-a8c5-c868c10a5cf1',
    title: 'Locale settings per user',
    description: 'Add settings configure the locale of the current user',
    priority: 'low',
    type: 'Feature'
}, {
    issueNo: 'e8f23e02-3427-4648-bf16-fb2b05e3169b',
    title: 'Add new customer tutorial',
    description: 'Create a tutorial on how to add a new customer into the application',
    priority: 'high',
    type: 'Documentation'
},];
