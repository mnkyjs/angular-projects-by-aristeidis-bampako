import { Injectable } from '@angular/core';
import { uuid } from '@ap/utils';
import dayjs from 'dayjs';
import { Issue } from '../models/issue';
import { issues } from '../testing/mock-issues';

@Injectable({
    providedIn: 'root'
})
export class IssuesService {
    private issues: Issue[] = [];

    constructor() {
        this.issues = issues;
    }

    getPendingIssues(): Issue[] {
        return this.issues.filter((issue: Issue) => !issue.completed);
    }

    getSuggestions(title: string): Issue[] {
        if (title.length > 3) {
            return this.issues.filter((issue: Issue) => issue.title.indexOf(title) !== -1);
        }
        return [];
    }

    createIssue(issue: Issue) {
        issue.issueNo = uuid();
        this.issues.push(issue);
    }

    completeIssue(issue: Issue) {
        const selectedIssue: Issue = {
            ...issue,
            completed: dayjs(),
        };

        const index = this.issues.findIndex((i: Issue) => i.issueNo === issue.issueNo);
        this.issues[index] = selectedIssue;
    }

    updateIssue(issue: Issue) {
        const index = this.issues.findIndex((i: Issue) => i === issue);
        console.table(index);
        this.issues = this.issues.map((i: Issue) => i.issueNo === issue.issueNo ? issue : i);
    }
}
