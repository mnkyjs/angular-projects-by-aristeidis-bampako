import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UiModule } from '@ap/ui';
import { ClarityModule } from '@clr/angular';

import { IssueListComponent } from './components/issue-list/issue-list.component';
import { IssueReportComponent } from './components/issue-report/issue-report.component';
import { ISSUE_ROUTES } from './issue.routes';

@NgModule({
    imports: [
        CommonModule,
        ClarityModule,
        ReactiveFormsModule,
        RouterModule.forChild(ISSUE_ROUTES),
        UiModule
    ],
    declarations: [IssueListComponent, IssueReportComponent],
    exports: [IssueListComponent],
})
export class IssuesModule {
}
