import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Issue } from '../../models/issue';

import { IssuesService } from '../../services/issues.service';

@Component({
    selector: 'ap-issue-report',
    templateUrl: './issue-report.component.html',
    styleUrls: ['./issue-report.component.scss'
    ]
})
export class IssueReportComponent implements OnInit, OnDestroy {
    @Input()
    issue: Issue | null = null;

    @Output()
    formClose = new EventEmitter();

    issueForm: FormGroup | undefined;

    suggestions: Issue[] = [];

    subs: Subscription = new Subscription();

    constructor(private formBuilder: FormBuilder, private issuesService: IssuesService) { }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }

    ngOnInit(): void {
        this.issueForm = this.formBuilder.group({
            issueNo: [this.issue?.issueNo],
            title: [this.issue?.title, Validators.required],
            description: [this.issue?.description],
            priority: [this.issue?.priority, Validators.required],
            type: [this.issue?.type, Validators.required]
        });

        this.subs.add(
            this.issueForm.controls['title'].valueChanges.subscribe((title: string) => {
                this.suggestions = this.issuesService.getSuggestions(title);
            })
        );
    }

    addIssue() {
        if (this.issueForm && this.issueForm.invalid) {
            this.issueForm.markAllAsTouched();
            return;
        }

        if (this.issue && this.issue.issueNo) {
            this.issuesService.updateIssue(this.issueForm?.value);
        } else {
            this.issuesService.createIssue(this.issueForm?.value);
        }

        this.formClose.emit();
    }
}
