import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'ap-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
    @Input()
    issueNo: string | null = null;

    @Output()
    confirm = new EventEmitter<boolean>();

    agree() {
        this.confirm.emit(true);
        this.issueNo = null;
    }

    disagree() {
        this.confirm.emit(false);
        this.issueNo = null;
    }
}
